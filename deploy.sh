#!/bin/bash

if test "$(basename $PWD)" != "Decaydata"; then
    echo "Need to run in Decaydata/ directory."
    exit 1;
fi

rm -f ../Decaydata.zip
zip -r ../Decaydata.zip ./ --exclude .git/\*
mv ../Decaydata.zip ../master/HADRONS++/
cp -rp ../master/HADRONS++/Decaydata.zip ../master/share/SHERPA-MC/

echo "Finished successfully."
